@extends('layouts.app')

@section('content')
    <div class="main-content" id="app">
        <section class="section">
            <h1 class="section-header">
                <div>Motor Vehicle Inspection Form</div>
            </h1>
            <div class="section-body">
                <form method="post" action="{{ route('transaction.vehicle') }}">
                    @csrf
                    <div class="row">

                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Ownership and Documentation</h4>
                                </div>
                                <div class="card-body">

                                        <div class="form-group">
                                            <label for="name">Owner's complete name and address <span class="text-danger">*</span></label>
                                            <input class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ Auth::user()->first_name . ' ' . Auth::user()->last_name }} ">
                                        </div>

                                        <div class="form-group">
                                            <label for="from">Acquired from (complete name and address)</label>
                                            <input class="form-control {{ $errors->has('from') ? ' is-invalid' : '' }}" name="from" id="from">
                                        </div>

                                        <div class="form-group">
                                            <label for="encumbrance">Encumbrance (Company name and address)</label>
                                            <input class="form-control {{ $errors->has('encumbrance') ? ' is-invalid' : '' }}" name="encumbrance" id="encumbrance">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Kind of vehicle <span class="text-danger">*</span></label>
                                            <div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="kov" id="kov" value="New" checked="checked">
                                                    <label class="form-check-label" for="inlineRadio1">New</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="kov" id="kov" value="2nd hand">
                                                    <label class="form-check-label" for="inlineRadio1">2nd hand</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="kov" id="kov" value="Rebuilt">
                                                    <label class="form-check-label" for="inlineRadio1">Rebuilt</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="kov" id="kov" value="Car">
                                                    <label class="form-check-label" for="inlineRadio1">Car</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="kov" id="kov" value="Truck">
                                                    <label class="form-check-label" for="inlineRadio1">Truck</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="kov" id="kov" value="Hire">
                                                    <label class="form-check-label" for="inlineRadio1">Hire</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="kov" id="kov" value="MC">
                                                    <label class="form-check-label" for="inlineRadio1">MC</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="kov" id="kov" value="TC">
                                                    <label class="form-check-label" for="inlineRadio1">TC</label>
                                                </div>
                                            </div>
                                        </div>

                                        <button class="btn btn-primary" type="submit" >Submit</button>

                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </section>
    </div>


@endsection

@push('scripts')
    <script src="{{ mix('/js/form.js') }}"></script>
@endpush
