<a href="#" onclick="window.print();return false;">			<span class="icon-print" aria-hidden="true"></span>
    Print	</a>

<div class="main-content" id="app">
    <section class="section">
        <h1 class="section-header">
            <div>Motor Vehicle Inspection Form</div>
        </h1>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>OWNERSHIP AND DOCUMENTATION</h4>
                        </div>
                        <div class="card-body">

                            <div class="form-group">
                                <label for="name">Owner's complete name and address</label>
                                <input class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ $vehicle_registration->name }} " readonly>
                            </div>

                            <div class="form-group">
                                <label for="from">Acquired from (complete name and address)</label>
                                <input class="form-control {{ $errors->has('from') ? ' is-invalid' : '' }}" name="from" id="from" value="{{ $vehicle_registration->from }} " readonly>
                            </div>

                            <div class="form-group">
                                <label for="encumbrance">Encumbrance (Company name and address)</label>
                                <input class="form-control {{ $errors->has('encumbrance') ? ' is-invalid' : '' }}" name="encumbrance" id="encumbrance" value="{{ $vehicle_registration->encumbrance }} " readonly>
                            </div>

                            <div class="form-group">
                                <label for="exampleFormControlInput1">Kind of vehicle</label>
                                <input class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ $vehicle_registration->kov }} " readonly>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>