<a href="#" onclick="window.print();return false;">			<span class="icon-print" aria-hidden="true"></span>
    Print	</a>

<div class="main-content" id="app">
    <section class="section">
        <h1 class="section-header">
            <div>Application for Driver's License</div>
        </h1>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="first_name">First Name</label>
                                                <input class="form-control" name="first_name" id="first_name" value="{{ $drivers_license->first_name }} " readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="last_name">Last Name</label>
                                                <input class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" id="last_name" value="{{ $drivers_license->last_name }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="middle_name">Middle Name</label>
                                                <input class="form-control" name="middle_name" id="middle_name" value="{{ $drivers_license->middle_name }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="address">Present Address (No., Street, City/Municipality, Province)</label>
                                                <input class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" id="address" value="{{ $drivers_license->address }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="tin">TIN</label>
                                                <input class="form-control" name="tin" id="tin" value="{{ $drivers_license->tin }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="tel_no">Tel no. / CP no.</label>
                                                <input class="form-control" name="tel_no" id="tel_no" value="{{ $drivers_license->tel_no }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="nationality">Nationality</label>
                                                <input class="form-control {{ $errors->has('nationality') ? ' is-invalid' : '' }}" name="nationality" id="nationality" value="{{ $drivers_license->nationality }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Gender</label>
                                                <input class="form-control" value="{{ $drivers_license->gender }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-4">
                                                        <label for="exampleFormControlInput1">Birth date</label>
                                                        <input class="form-control" value="{{ $drivers_license->dob }}" readonly>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Weight (kg)</label>
                                                <input type="number" min="0" class="form-control {{ $errors->has('weight') ? ' is-invalid' : '' }}" name="weight" id="weight" value="{{ $drivers_license->weight }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Height (cm)</label>
                                                <input type="number" min="0" class="form-control {{ $errors->has('height') ? ' is-invalid' : '' }}" name="height" id="height" value="{{ $drivers_license->height }}" readonly>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="middle_name">Type of Application (TOA)</label>
                                            <input class="form-control" value="{{ $drivers_license->toa }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="middle_name">Type of License Applied for (TLA)</label>
                                            <input class="form-control" value="{{ $drivers_license->tla }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="middle_name">Driving skill acquired or will be acquired thru (DSA)</label>
                                            <input class="form-control" value="{{ $drivers_license->dsa }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="middle_name">Educational Attainment (EA)</label>
                                            <input class="form-control" value="{{ $drivers_license->ea }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Blood type</label>
                                                <input class="form-control" name="blood_type" id="blood_type" value="{{ $drivers_license->blood_type }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Organ Donor</label>
                                                <input class="form-control" value="{{ $drivers_license->organ_donor }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="middle_name">Civil Status (SC)</label>
                                            <input class="form-control" value="{{ $drivers_license->cs }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="middle_name">Hair</label>
                                            <input class="form-control" value="{{ $drivers_license->hair }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="middle_name">Eyes</label>
                                            <input class="form-control" value="{{ $drivers_license->eyes }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="middle_name">Built</label>
                                            <input class="form-control" value="{{ $drivers_license->built }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <label for="middle_name">Complexion</label>
                                            <input class="form-control" value="{{ $drivers_license->complexion }}" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Birth place</label>
                                                <input class="form-control {{ $errors->has('birth_place') ? ' is-invalid' : '' }}" name="birth_place" id="birth_place" value="{{ $drivers_license->birth_place }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Father's name</label>
                                                <input class="form-control {{ $errors->has('fathers_name') ? ' is-invalid' : '' }}" name="fathers_name" id="exampleFormControlInput1" value="{{ $drivers_license->fathers_name }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Mother's name</label>
                                                <input class="form-control {{ $errors->has('mothers_name') ? ' is-invalid' : '' }}" name="mothers_name" id="exampleFormControlInput1" value="{{ $drivers_license->mothers_name }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Spouse name</label>
                                                <input class="form-control {{ $errors->has('spouse_name') ? ' is-invalid' : '' }}" name="spouse_name" id="exampleFormControlInput1" value="{{ $drivers_license->spouse_name }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Employer's Business Name</label>
                                                <input class="form-control {{ $errors->has('emp_bus_name') ? ' is-invalid' : '' }}" name="emp_bus_name" id="exampleFormControlInput1" value="{{ $drivers_license->emp_bus_name }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Tel no.</label>
                                                <input class="form-control {{ $errors->has('emp_tel_no') ? ' is-invalid' : '' }}" name="emp_tel_no" id="exampleFormControlInput1" value="{{ $drivers_license->emp_tel_no }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Employer's business address</label>
                                                <input class="form-control {{ $errors->has('emp_bus_add') ? ' is-invalid' : '' }}" name="emp_bus_add" id="exampleFormControlInput1" value="{{ $drivers_license->emp_bus_add }}" readonly>
                                            </div>

                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Previous Name</label>
                                                <input class="form-control {{ $errors->has('prev_name') ? ' is-invalid' : '' }}" name="prev_name" id="exampleFormControlInput1" value="{{ $drivers_license->prev_name }}" readonly>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
