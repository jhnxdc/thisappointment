@extends('layouts.app')

@section('content')
    <div class="main-content" id="app">
        <section class="section">
            <h1 class="section-header">
                <div>Transaction Type</div>
            </h1>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Select one</h4>
                            </div>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-lg-4 col-md-6 col-12">
                                        <a href="{{ route('user.license') }}">
                                            <div class="card card-sm-3">
                                                <div class="card-icon bg-primary">
                                                    <i class="ion ion-card"></i>
                                                </div>
                                                <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4>Application for Driver's License</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>

                                    <div class="col-lg-4 col-md-6 col-12">
                                        <a href="{{ route('user.vehicle') }}">
                                            <div class="card card-sm-3">
                                                <div class="card-icon bg-danger">
                                                    <i class="ion ion-android-car"></i>
                                                </div>
                                                <div class="card-wrap">
                                                    <div class="card-header">
                                                        <h4>Vehicle Registration</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


@endsection

@push('scripts')
    <script src="{{ mix('/js/form.js') }}"></script>
@endpush
