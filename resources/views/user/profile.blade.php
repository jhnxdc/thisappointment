@extends('layouts.app')

@section('content')
    <div class="main-content" id="app">
        <section class="section">
            <h1 class="section-header">
                <div>Profile</div>
            </h1>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Basic Info</h4>
                            </div>
                            <div class="card-body">

                                <form method="post" action="{{ route('profile') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="first_name">First Name <span class="text-danger">*</span></label>
                                        <input class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" id="first_name" value="{{ Auth::user()->first_name }} ">
                                    </div>

                                    <div class="form-group">
                                        <label for="last_name">Last Name <span class="text-danger">*</span></label>
                                        <input class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" id="last_name" value="{{ Auth::user()->last_name }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="middle_name">Middle Name</label>
                                        <input class="form-control" name="middle_name" id="middle_name" value="{{ Auth::user()->middle_name }}">
                                    </div>

                                    <button class="btn btn-primary" >Save</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Email</h4>
                            </div>
                            <div class="card-body">

                                <form method="post" action="{{ route('profile.email') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="first_name">Email <span class="text-danger">*</span></label>
                                        <input class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="email" value="{{ Auth::user()->email }} ">

                                        @if ($errors->has('email'))
                                            <div class="invalid-feedback">
                                                {{ $errors->first('email') }}
                                            </div>
                                        @endif
                                    </div>

                                    <button class="btn btn-primary" >Save</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Password</h4>
                            </div>
                            <div class="card-body">

                                <form method="post" action="{{ route('profile.password') }}">
                                    @csrf

                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label for="first_name">Password <span class="text-danger">*</span></label>
                                                <input class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" id="password" type="password">

                                                @if ($errors->has('password'))
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('password') }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label for="first_name">Password Confirmation<span class="text-danger">*</span></label>
                                                <input class="form-control" name="password_confirmation" id="password_confirmation" type="password">
                                            </div>
                                        </div>
                                    </div>

                                    <button class="btn btn-primary" >Save</button>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>


@endsection

@push('scripts')
    <script src="{{ mix('/js/form.js') }}"></script>
@endpush
