@extends('layouts.app')

@section('content')
    <div class="main-content" id="app">
        <section class="section">
            <h1 class="section-header">
                <div>Application for Driver's License</div>
            </h1>
            <div class="section-body">
                <form method="POST" action="{{ route('transaction.license') }}">
                    @csrf

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Fill up form</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label for="first_name">First Name <span class="text-danger">*</span></label>
                                                <input class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" id="first_name" value="{{ Auth::user()->first_name }} ">
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label for="last_name">Last Name <span class="text-danger">*</span></label>
                                                <input class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" id="last_name" value="{{ Auth::user()->last_name }}">
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-6">
                                            <div class="form-group">
                                                <label for="middle_name">Middle Name</label>
                                                <input class="form-control" name="middle_name" id="middle_name" value="{{ Auth::user()->middle_name }}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="address">Present Address (No., Street, City/Municipality, Province) <span class="text-danger">*</span></label>
                                        <input class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" id="address">
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <label for="tin">TIN</label>
                                                <input class="form-control" name="tin" id="tin">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <label for="tel_no">Tel no. / CP no.</label>
                                                <input class="form-control" name="tel_no" id="tel_no">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12 col-md-4">
                                            <div class="form-group">
                                                <label for="nationality">Nationality <span class="text-danger">*</span></label>
                                                <input class="form-control {{ $errors->has('nationality') ? ' is-invalid' : '' }}" name="nationality" id="nationality">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Gender <span class="text-danger">*</span></label>
                                        <div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="gender" id="gender" value="Male" checked="checked">
                                                <label class="form-check-label" for="inlineRadio1">Male</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="gender" id="gender" value="Female">
                                                <label class="form-check-label" for="inlineRadio2">Female</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-4">
                                                <label for="exampleFormControlInput1">Birth date <span class="text-danger">*</span></label>
                                                <div class="input-group date" data-provide="datepicker" id="datepicker">
                                                    <input type="text" class="form-control {{ $errors->has('dob') ? ' is-invalid' : '' }}" name="dob">
                                                    <div class="input-group-addon">
                                                        <span class="glyphicon glyphicon-th"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12 col-md-2">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Weight (kg)</label>
                                                <input type="number" min="0" class="form-control {{ $errors->has('weight') ? ' is-invalid' : '' }}" name="weight" id="weight">
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-2">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Height (cm)</label>
                                                <input type="number" min="0" class="form-control {{ $errors->has('height') ? ' is-invalid' : '' }}" name="height" id="height">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Type of Application (TOA) <span class="text-danger">*</span></h4>
                                </div>
                                <div class="card-body">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="New" checked="checked">
                                        <label class="form-check-label" for="exampleRadios2">
                                            New
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Delinquent/Dormant license">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Delinquent/Dormant license
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Prof to Non-Prof">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Prof to Non-Prof
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Non-Prof to Prof">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Non-Prof to Prof
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Foreign Lic. Conversion">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Foreign Lic. Conversion
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Renewal">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Renewal
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Additional Restriction Code">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Additional Restriction Code
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Duplicate">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Duplicate
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Revision of Records">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Revision of Records
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Change Address">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Change Address
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Change Civil Status">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Change Civil Status
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Change Name">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Change Name
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Change Date of Birth">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Change Date of Birth
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Others">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Others
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Type of License Applied for (TLA) <span class="text-danger">*</span></h4>
                                </div>
                                <div class="card-body">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="tla" id="exampleRadios2" value="Student Permit" checked="checked">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Student Permit
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="tla" id="exampleRadios2" value="Non-professional">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Non-professional
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="tla" id="exampleRadios2" value="Professional">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Professional
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="tla" id="exampleRadios2" value="Conductor">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Conductor
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Driving skill acquired or will be acquired thru (DSA) <span class="text-danger">*</span></h4>
                                </div>
                                <div class="card-body">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dsa" id="exampleRadios2" value="Driving School" checked="checked">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Driving School
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="dsa" id="exampleRadios2" value="Licensed Private Person">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Licensed Private Person
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Educational Attainment (EA) <span class="text-danger">*</span></h4>
                                </div>
                                <div class="card-body">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="ea" id="exampleRadios2" value="Informal Schooling" checked="checked">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Informal Schooling
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="ea" id="exampleRadios2" value="Elementary">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Elementary
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="ea" id="exampleRadios2" value="High School">
                                        <label class="form-check-label" for="exampleRadios2">
                                            High School
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="ea" id="exampleRadios2" value="Vocational">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Vocational
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="ea" id="exampleRadios2" value="College">
                                        <label class="form-check-label" for="exampleRadios2">
                                            College
                                        </label>
                                    </div>

                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="ea" id="exampleRadios2" value="Post Graduate">
                                        <label class="form-check-label" for="exampleRadios2">
                                            Post Graduate
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4></h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-2">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Blood type <span class="text-danger">*</span></label>
                                                <input class="form-control" name="blood_type" id="blood_type">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Organ Donor <span class="text-danger">*</span></label>
                                        <div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="organ_donor" id="inlineRadio1" value="Yes" checked="checked">
                                                <label class="form-check-label" for="inlineRadio1">Yes</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="organ_donor" id="inlineRadio2" value="No">
                                                <label class="form-check-label" for="inlineRadio2">No</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Civil Status (SC) <span class="text-danger">*</span></h4>
                                </div>
                                <div class="card-body">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="cs" id="inlineRadio1" value="Single" checked="checked">
                                        <label class="form-check-label" for="inlineRadio1">Single</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="cs" id="inlineRadio1" value="Married">
                                        <label class="form-check-label" for="inlineRadio1">Married</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="cs" id="inlineRadio1" value="Widower">
                                        <label class="form-check-label" for="inlineRadio1">Widower</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="cs" id="inlineRadio1" value="Separated">
                                        <label class="form-check-label" for="inlineRadio1">Separated</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Hair <span class="text-danger">*</span></h4>
                                </div>
                                <div class="card-body">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="hair" id="inlineRadio1" value="Black" checked="checked">
                                        <label class="form-check-label" for="inlineRadio1">Black</label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="hair" id="inlineRadio1" value="Brown">
                                        <label class="form-check-label" for="inlineRadio1">Brown</label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="hair" id="inlineRadio1" value="Blonde">
                                        <label class="form-check-label" for="inlineRadio1">Blonde</label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="hair" id="inlineRadio1" value="Gray">
                                        <label class="form-check-label" for="inlineRadio1">Gray</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Eyes <span class="text-danger">*</span></h4>
                                </div>
                                <div class="card-body">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="eyes" id="inlineRadio1" value="Black" checked="checked">
                                        <label class="form-check-label" for="inlineRadio1">Black</label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="eyes" id="inlineRadio1" value="Brown">
                                        <label class="form-check-label" for="inlineRadio1">Brown</label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="eyes" id="inlineRadio1" value="Gray">
                                        <label class="form-check-label" for="inlineRadio1">Gray</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Built <span class="text-danger">*</span></h4>
                                </div>
                                <div class="card-body">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="built" id="inlineRadio1" value="Light" checked="checked">
                                        <label class="form-check-label" for="inlineRadio1">Light</label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="built" id="inlineRadio1" value="Medium">
                                        <label class="form-check-label" for="inlineRadio1">Medium</label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="built" id="inlineRadio1" value="Heavy">
                                        <label class="form-check-label" for="inlineRadio1">Heavy</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Complexion <span class="text-danger">*</span></h4>
                                </div>
                                <div class="card-body">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="complexion" id="inlineRadio1" value="Light" checked="checked">
                                        <label class="form-check-label" for="inlineRadio1">Light</label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="complexion" id="inlineRadio1" value="Fair">
                                        <label class="form-check-label" for="inlineRadio1">Fair</label>
                                    </div>

                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="complexion" id="inlineRadio1" value="Dark">
                                        <label class="form-check-label" for="inlineRadio1">Dark</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4></h4>
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Birth place <span class="text-danger">*</span></label>
                                        <input class="form-control {{ $errors->has('birth_place') ? ' is-invalid' : '' }}" name="birth_place" id="birth_place">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Father's name</label>
                                        <input class="form-control {{ $errors->has('fathers_name') ? ' is-invalid' : '' }}" name="fathers_name" id="exampleFormControlInput1">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Mother's name</label>
                                        <input class="form-control {{ $errors->has('mothers_name') ? ' is-invalid' : '' }}" name="mothers_name" id="exampleFormControlInput1">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Spouse name</label>
                                        <input class="form-control {{ $errors->has('spouse_name') ? ' is-invalid' : '' }}" name="spouse_name" id="exampleFormControlInput1">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Employer's Business Name</label>
                                        <input class="form-control {{ $errors->has('emp_bus_name') ? ' is-invalid' : '' }}" name="emp_bus_name" id="exampleFormControlInput1">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Tel no.</label>
                                        <input class="form-control {{ $errors->has('emp_tel_no') ? ' is-invalid' : '' }}" name="emp_tel_no" id="exampleFormControlInput1">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Employer's business address</label>
                                        <input class="form-control {{ $errors->has('emp_bus_add') ? ' is-invalid' : '' }}" name="emp_bus_add" id="exampleFormControlInput1">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Previous Name</label>
                                        <input class="form-control {{ $errors->has('prev_name') ? ' is-invalid' : '' }}" name="prev_name" id="exampleFormControlInput1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <button class="btn btn-primary" >Submit</button>
                </form>
            </div>
        </section>
    </div>


@endsection

@push('scripts')
    <script src="{{ mix('/js/form.js') }}"></script>
@endpush
