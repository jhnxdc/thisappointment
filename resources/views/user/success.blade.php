@extends('layouts.app')

@section('content')
    <div class="main-content" id="app">
        <section class="section">
            <h1 class="section-header">
                <div>Success!</div>
            </h1>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                @if($transaction->transaction_type_id == 1)
                                <h4>Application for Driver's License</h4>
                                @elseif($transaction->transaction_type_id == 2)
                                <h4>Vehicle Registration</h4>
                                @endif
                            </div>
                            <div class="card-body">
                                <h3>You will receive an email containing the schedule of your appointment</h3>
                            </div>
                        </div>
                    </div>
                </div>

            @if($transaction->transaction_type_id == 1)
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Requirements</h4>
                            </div>
                            <div class="card-body">
                                <a href="{{ route('print.license') }}" target="_blank" class="btn btn-primary">Print</a>
                                <br/><br/>
                                <li>New Professional Driver's License / Change Classification from Non-Professional to Professional Driver's License</li>
                                <li>Conductor's License</li>
                                <li>Renewal of Professional Driver's License / Conductor's License</li>
                                <li>Additional Restriction Codes</li>
                                <li>Revision of Records</li>
                                </ul>
                                <p><strong>QUALIFICATIONS</strong></p>
                                <ol>
                                    <li>Must be at least eighteen (18) years old;</li>
                                    <li>Must be physically and mentally fit to operate a motor vehicle (or work, in the case of applications for Conductor's License);</li>
                                    <li>Must be able to read and write Filipino, English or the applicable local dialect;</li>
                                    <li>For RC 1,2,4 and 6 - must be a holder of valid Student Permit (SP) issued at least six (6) months prior to the application.<br />For RC 3,5,7 and 8 - must be a holder of valid Non-Professional Driver's License (NPDL) at least one (1) year prior to the&nbsp;application or PDL holder with RC 1,2,4 and 6 for a period of at least six (6) months prior to the application.</li>
                                    <li>Must not have been cited for two (2) or more counts of reckless driving during the period of validity of his/her existing license; and</li>
                                    <li>In addition, for foreigners, the applicant must have been in the Philippines for at least one (1) month with proof that he/she will stay in the country&nbsp;<span style="color: #000000;">for</span>&nbsp;at least one (1) year from date of application and is allowed to work herein.</li>
                                </ol>
                                <p><span style="color: #000000;"><strong>REQUIREMENTS</strong></span></p>
                                <p><strong>1.&nbsp;<span style="text-decoration: underline;">NEW PROFESSIONAL DRIVER'S LICENSE OR CONDUCTOR'S LICENSE/ CHANGE CLASSIFIFCATION FROM NPDL TO PDL</span></strong></p>
                                <ol style="list-style-type: lower-alpha;">
                                    <li>Duly accomplished Application for Driver's License (ADL);</li>
                                    <li>For RC 1,2,4 and 6 - Must be a holder of a valid SP issued at least six (6) months prior to the application.<br />For RC 3,5,7 and 8 - Must be a holder of a valid NPDL issued at least one (1) year prior to the application or PDL&nbsp;holder with RC 1,2,4 and 6 for a period of at least six (6) months prior to the application.</li>
                                    <li>Medical Certificate issued by any licensed practicing physician stating that the applicant is physically and mentally&nbsp;fit to operate a motor vehicle (or work, in the case of applications for Conductor's License);</li>
                                    <li>Clearance that the applicant has not been convicted of any offense involving moral turpitude or reckless imprudence resulting from reckless driving from all the following:<br />&bull; National Bureau of Investigation (NBI); and<br />&bull; Philippine national Police (PNP)</li>
                                    <li>In addition, for foreigners, original and photocopy of passport with entry of at least one (1) year from date of application, or if born in the&nbsp;<span style="color: #000000;">Philippines, present original and</span>&nbsp;photocopy of birth certificate duly authenticated by the NSO.</li>
                                </ol>
                                <p><strong>2.&nbsp;<span style="text-decoration: underline;">RENEWAL</span></strong></p>
                                <ol style="list-style-type: lower-alpha;">
                                    <li>Duly Accomplished Application for Driver's License (ADL);</li>
                                    <li>Professional Driver's License not expired for more that ten (10) years;</li>
                                    <li>Medical Certificate issued by any licensed practicing physician stating that the applicant is physically and mentally fit to operate a motor vehicle&nbsp;<span style="color: #000000;">or otherwise stating his/her impairment.</span></li>
                                </ol>
                                <p><strong>3.&nbsp;<span style="text-decoration: underline;">ADDITIONAL RESTRICTION CODES</span></strong></p>
                                <ol style="list-style-type: lower-alpha;">
                                    <li>Duly Accomplished Application for Driver's License (ADL);</li>
                                    <li><span style="color: #000000;">Valid&nbsp;</span>Professional Driver's License (PDL);</li>
                                    <li>Medical Certificate issued by any licensed practicing physician stating that the applicant is physically and mentally fit to operate a motor vehicle or otherwise stating his/her&nbsp;<span style="color: #000000;">impairment.</span></li>
                                </ol>
                                <p><strong>4.&nbsp;<span style="text-decoration: underline;">REVISION OF RECORDS</span></strong></p>
                                <ol style="list-style-type: lower-alpha;">
                                    <li>Duly Accomplished Application for Driver's License (ADL);</li>
                                    <li>Valid Professional Driver's License (PDL);</li>
                                    <li>In case of change/s in name, date of birth and/or civil status,original and photocopy of birth certificate or marriage contract, as the case may be, duly authenticated by the NSO.</li>
                                </ol>
                                <p><span style="color: #000000;"><strong>PROCEDURE</strong></span></p>
                                <p><strong>1.&nbsp;<span style="text-decoration: underline;">NEW PROFESSIONAL DRIVER'S LICENSE OR CONDUCTOR'S LICENSE/ADDITIONAL RESTRICTION CODES</span></strong></p>
                                <p><strong>Step 1</strong></p>
                                <p>The applicant shall submit all the documentary requirements to the receiving personnel.</p>
                                <ul>
                                    <li>Receiving personnel shall check only the completeness of the documents required and transmit the same to an evaluator.</li>
                                    <li>The evaluator shall evaluate the qualifications of the applicant and, if qualified, encode the application in the system.</li>
                                </ul>
                                <p><strong>Step 2</strong></p>
                                <p>The applicant shall be called for photo and signature taking.</p>
                                <p><strong>Step 3</strong></p>
                                <p>The applicant shall be called to pay the required fees to the cashier.</p>
                                <p><strong>Step 4</strong></p>
                                <p>The applicant shall immediately proceed to undergo written examinations in the form of the PDL Basic Driving Theory&nbsp;Test for the RC/s applied for or Conductor's License examination and will be immediately informed of the results of the same.</p>
                                <p><strong>Step 5</strong></p>
                                <p>Except for applications for Conductor's License, an applicant who passes the written examinations shall be required to&nbsp;immediately take the practical driving test for the RC/s applied for.</p>
                                <p>After passing both examinations, the application shall be approved by the Approving Officer.</p>
                                <ul>
                                    <li>Applicants who fail to pass the Conductor's License Examintaions, Basic Driving Theory Test and/or the Practical Driving Test shall not be allowed to take the same test within a period of one (1) month from the date of the last examination.</li>
                                    <li>If the applicants fails to pass any of the written or practical examinations twice within a period of one (1) year, he/she shall not be allowed to apply for the same within a period of one (1) year from the date of the last examination.</li>
                                    <li>If applicant fails to pass any of the written or practical examinations thrice, he/she shall no longer be allowed to apply for the same for a period of two (2) years from the time of the latest failure. However, in case the application is only for additional RC/s, he/she shall be allowed to renew his/her PDL or Conductor's License and, for this purpose, such shall be annotated on the record of the applicant.</li>
                                </ul>
                                <p><strong>Step 6</strong></p>
                                <p>Upon approval, the PDL or Conductor's License card with the Official Receipt shall be released to the applicant, who shall&nbsp;be required to write his/her name and affix his/her signature in the appropriate Release Form.</p>
                                <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>
                                <p><strong>2.&nbsp;<span style="text-decoration: underline;">RENEWAL/REVISION OF RECORD</span></strong></p>
                                <p><strong>Step 1</strong></p>
                                <p>The applicant shall submit all the documentary requirements to the receiving personnel.</p>
                                <ul>
                                    <li>Receiving personnel shall retrieve the details of the license from the system and show the same to the applicant, who shall be required to confirm the same for any revision/s and be informed of the appropriate fees to be paid.</li>
                                    <li>If there are no revision/s to be made, applicant shall be advised to proceed to the next step.</li>
                                    <li>If there are revision/s to be made, the applicant shall be required to fill up an ADL for the desired revision/s attaching therewith the appropriate document/s required if necessary, for re-submission. For this purpose, ADL forms must be made readily available for applicant/s at the same receiving window.</li>
                                </ul>
                                <p>In case of revision/s of records only, the receiving personnel shall check only the completeness of the re-submitted documents and<br />transmit the same to an evaluator.</p>
                                <ul>
                                    <li>The evaluator shall evaluate the qualification of the applicant and, if qualified, encode the application in the file system.</li>
                                    <li>Upon encoding, the application shall be approved by the Approving Officer.</li>
                                </ul>
                                <p><strong>Step 2</strong></p>
                                <p>The applicant shall be called for photo and signature taking.</p>
                                <p><strong>Step 3</strong></p>
                                <p>The applicant shall be called to pay the required fees to the Cashier.</p>
                                <p><strong>Step 4</strong></p>
                                <p>The professional Driver's License or Conductor's License card and the expired card with the Official Receipt shall be released&nbsp;to the applicant, who shall be required to write his/her name and affix his/her signature in the appropriate Release Form.</p>
                                <p><strong>Venue:</strong></p>
                                <ol style="list-style-type: upper-alpha;">
                                    <li>Applications for new PDL as well as its subsequent transactions may be done at any LTO Licensing Center and District/Extension Office.</li>
                                    <li>The plain renewal of PDL without revision may be done at any LTO Driver's Licensing Renewal Offices (DLROs)</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if($transaction->transaction_type_id == 2)
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Requirements</h4>
                            </div>
                            <div class="card-body">
                                <a href="{{ route('print.vehicle') }}" target="_blank" class="btn btn-primary">Print</a>
                                <br/><br/>

                                <p><span style="font-size: 12pt;"><strong>Initial Registration</strong></span></p>
                                <p><strong>Brand New</strong></p>
                                <ul>
                                    <li>Locally assembled/manufactured Completely Built Units (CBUs)</li>
                                    <li>Imported CBUs</li>
                                </ul>
                                <p><strong>Others</strong></p>
                                <ul>
                                    <li>Imported used CBUs</li>
                                    <li>Rebuilt Motor Vehicles&nbsp;</li>
                                    <li>Imported CBUs acquired through public bidding conducted by the Bureau of Customs (BOC).</li>
                                </ul>
                                <p><strong>Special</strong></p>
                                <ul>
                                    <li>Underbond MVs</li>
                                    <li>MVs under Written Commitment</li>
                                </ul>
                                <p>&nbsp;</p>
                                <p><strong>Documentary Requirements:</strong></p>
                                <p><strong>A. BRAND NEW LOCALLY ASSEMBLED/MANUFACTURED COMPLETELY BUILT UNITS (CBUs)</strong></p>
                                <ol>
                                    <li>Original Sales Invoice</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>Original Certificate of Stock Reported (CSR)</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                </ol>
                                <p><span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p><strong>B. BRAND NEW IMPORTED CBUs</strong></p>
                                <ol>
                                    <li>Original Sales Invoice or Commercial Invoice issued by the Country of Origin</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>Original Certificate of Stock Reported (CSR)</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                </ol>
                                <p><strong></strong><span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p><strong>C. BRAND NEW LOCAL/IMPORTED TRAILER</strong></p>
                                <ol>
                                    <li>Original Sales Invoice or Commercial Invoice issued by the Country of Origin</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>Original Certificate of Stock Reported (CSR)</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                </ol>
                                <p><span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p><strong>D. TAX EXEMPT</strong></p>
                                <ol>
                                    <li>Commercial Invoice of Motor Vehicle or Certificate of Title issued by the Country of Origin</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>BOC Certificate of Payment</li>
                                    <li>Certified True Copy of Tax Exemption Certificate</li>
                                    <li>Original Certificate of Stock Reported (CSR)</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                    <li>Motor Vehicle Inspection Report (MVIR) if imported second hand</li>
                                    <li>Certificate of Compliance to Emission Standards (CCES) if imported second hand</li>
                                </ol>
                                <p><span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p><strong>E. IMPORTED SECOND HAND (USED) EXEMPTED FROM EO 156/877-A</strong></p>
                                <ol>
                                    <li>Original Sales Invoice and/or Commercial Invoice of Motor Vehicle/ Certificate&nbsp;of Title issued by the Country of Origin</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>Original Certificate of Stock Reported (CSR)</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                    <li>Motor Vehicle Inspection Report (MVIR)</li>
                                    <li>Certificate of Compliance to Emission Standards (CCES)</li>
                                </ol>
                                <p><span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p><strong>F. IMPORTED SECOND HAND THROUGH THE NO DOLLAR IMPORTATION</strong></p>
                                <ol>
                                    <li>Commercial Invoice of Motor Vehicle or Certificate of Title issued by the&nbsp;Country of Origin</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>Certified True Copy of the Authority under the No Dollar Importation issued by the DTI BIS<br />If no authority from BIS, Seizure Proceedings and Notice of Award.</li>
                                    <li>Original Affidavit of first and last importation</li>
                                    <li>Original Certificate of Stock Reported (CSR)</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                    <li>Motor Vehicle Inspection Report (MVIR)</li>
                                    <li>Certificate of Compliance to Emission Standards (CCES)</li>
                                </ol>
                                <p><span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p><strong>G. IMPORTED ACQUIRED THROUGH DONATION</strong></p>
                                <ol>
                                    <li>Commercial Invoice of Motor Vehicle or Certificate of Title issued by the Country of Origin</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>Original Deed of Donation duly signed by donor and the donee</li>
                                    <li>Original Secretary’s Certificate/Board Resolution when donor is a corporation</li>
                                    <li>Original Certificate of Stock Reported (CSR)</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                    <li>Motor Vehicle Inspection Report (MVIR)</li>
                                    <li>Certificate of Compliance to Emission Standards (CCES)</li>
                                </ol>
                                <p><span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p><strong>H. LOCALLY MANUFACTURED/ASSEMBLED ELECTRIC VEHICLES (NEW CHASSIS/BODY AND WITH NEW IMPORTED ELECTRIC MOTOR)</strong></p>
                                <ol>
                                    <li>Original Sales Invoice</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>Original Certificate of Stock Reported (CSR) for Electric Motor and Chassis</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                </ol>
                                <p><span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p><strong>I. BRAND NEW MOTORCYCLE WITH SIDECAR (TC)</strong></p>
                                <ol>
                                    <li>Original Sales Invoice</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>Original Affidavit of Attachment for Sidecar executed by the Owner and Mechanic stating among others the date of completion</li>
                                    <li>Original Certificate of Stock Reported (CSR)</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                </ol>
                                <p><span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p><strong>J. REBUILT WITH LOCAL CHASSIS/BODY AND WITH USED IMPORTED ENGINE</strong></p>
                                <ol>
                                    <li>Original Sales Invoice of Engine, Chassis &amp; Body</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>Original Affidavit of Rebuilt executed by the Owner and/or Mechanic with TESDA NC II stating among others the date of completion</li>
                                    <li>Original Certificate of Stock Reported (CSR) for Engine and Chassis</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                    <li>Motor Vehicle Inspection Report (MVIR)</li>
                                    <li>Certificate of Compliance to Emission Standards (CCES)</li>
                                </ol>
                                <p><span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p><strong>K. REBUILT WITH ENGINE AND/OR CHASSIS THAT ARE PARTS OF PREVIOUSLY REGISTERED MOTOR VEHICLES</strong></p>
                                <ol>
                                    <li>Original Sales Invoice of Body</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>Original Certificate of Registration and Latest Official Receipt of payment of registration fees of the engine or chassis</li>
                                    <li>Original Affidavit of Rebuilt executed by the Owner and Mechanic with TESDA NC II stating among others the date of completion</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                    <li>Motor Vehicle Inspection Report (MVIR)</li>
                                    <li>Certificate of Compliance to Emission Standards (CCES)</li>
                                </ol>
                                <p><span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p><strong>L. REBUILT TRUCK/BUS WITH NEW OR USED IMPORTED ENGINE AND/OR CHASSIS</strong></p>
                                <ol>
                                    <li>Commercial Invoice or Original Sales Invoice from Country of Origin</li>
                                    <li>Original Sales Invoice of Body</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>Certified True Copy of DTI-BIS Endorsement</li>
                                    <li>Original Affidavit of Rebuilt executed by the Owner and/or Mechanic with TESDA NC II stating among others the date of completion</li>
                                    <li>Original Certificate of Stock Reported (CSR) for Engine and Chassis</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                    <li>Motor Vehicle Inspection Report (MVIR)</li>
                                    <li>Certificate of Compliance to Emission Standards (CCES)</li>
                                </ol>
                                <p><span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p><strong>M. REBUILT TRUCK/BUS WITH NEW OR USED IMPORTED ENGINE AND/OR CHASSIS COMBINED WITH A PREVIOUSLY REGISTERED COMPONENT</strong></p>
                                <ol>
                                    <li>Original Sales Invoice of Body</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>Certified True Copy of DTI-BIS Endorsement</li>
                                    <li>Original Certificate of Registration and Latest Official Receipt of payment of registration fees (for previously registered engine and/or chassis)</li>
                                    <li>Original Affidavit of Rebuilt executed by the Owner and Mechanic with TESDA NC II stating among others the date of completion</li>
                                    <li>Original Certificate of Stock Reported (CSR) for Engine and Chassis</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                    <li>Motor Vehicle Inspection Report (MVIR)</li>
                                    <li>Certificate of Compliance to Emission Standards (CCES)</li>
                                </ol>
                                <p><span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p><strong>N. REBUILT TRUCK/BUS WITH ENGINE AND CHASSIS PURCHASED LOCALLY OR FROM PREVIOUSLY REGISTERED VEHICLES OR COMBINATION THEREOF</strong></p>
                                <ol>
                                    <li>Original Sales Invoice of Engine and Chassis</li>
                                    <li>Original Sales Invoice of Body</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>Original Certificate of Registration and Latest Official Receipt of payment of registration fees (for previously registered engine and/or chassis)</li>
                                    <li>Certified True Copy of DTI-BIS Endorsement</li>
                                    <li>Original Affidavit of Rebuilt executed by the Owner and/or Mechanic with TESDA NC II stating among others the date of completion</li>
                                    <li>Original Certificate of Stock Reported (CSR) for Engine and Chassis</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                    <li>Motor Vehicle Inspection Report (MVIR)</li>
                                    <li>Certificate of Compliance to Emission Standards (CCES)</li>
                                </ol>
                                <p><span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p><strong>O. IMPORTED MOTOR VEHICLES ACQUIRED THROUGH PUBLIC BIDDING</strong></p>
                                <ol>
                                    <li>Certified True Copy BOC official receipt evidencing payment of acquisition cost</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>Certified True Copy Decision on Seizure Proceedings</li>
                                    <li>Certified True Copy Notice of Award</li>
                                    <li>Original Certificate of Stock Reported (CSR)</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                    <li>Motor Vehicle Inspection Report (MVIR)</li>
                                    <li>Certificate of Compliance to Emission Standards (CCES)</li>
                                </ol>
                                <p><span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p><strong>P. UNDER BOND MOTOR VEHICLES</strong></p>
                                <ol>
                                    <li>Certificate of Title or Commercial Invoice issued by the country of origin</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>Duplicate Copy of the Certificate of Payment issued by the BOC</li>
                                    <li>Certified True Copy Certificate of release of the motor vehicle by the authority of the Commissioner</li>
                                    <li>Certified True Copy of the Re-export Bond duly approved by the Bureau of Customs to determine the effectivity date and duration of the motor vehicles temporary stay in the country</li>
                                    <li>Bill of Lading</li>
                                    <li>Original Certificate of Stock Reported (CSR)</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                    <li>Motor Vehicle Inspection Report (MVIR)</li>
                                    <li>Certificate of Compliance to Emission Standards (CCES)</li>
                                </ol>
                                <p><span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>
                                <p>&nbsp;</p>
                                <p><strong>Q. MOTOR VEHICLES UNDER WRITTEN COMMITMENT</strong></p>
                                <ol>
                                    <li>Certificate of Title or Commercial Invoice issued by the country of origin</li>
                                    <li>Appropriate Insurance Certificate of Cover</li>
                                    <li>Duplicate Copy of the Certificate of Payment issued by the BOC</li>
                                    <li>Certified True Copy of the written commitment duly approved by the Bureau of Custom to determine the effectivity date and duration of the motor vehicles temporary stay in the country</li>
                                    <li>Endorsement from Department of Foreign Affairs if the motor vehicle is embassy owned</li>
                                    <li>Certified True Copy of the Bill of Lading</li>
                                    <li>Original Certificate of Stock Reported (CSR)</li>
                                    <li>Original PNP-HPG MV Clearance Certificate</li>
                                    <li>Motor Vehicle Inspection Report (MVIR)</li>
                                    <li>Certificate of Compliance to Emission Standards (CCES)</li>
                                </ol>
                                <p>&nbsp;<span style="font-size: 10pt;"><strong>Procedures</strong></span></p>
                                <ol>
                                    <li>Proceed to the transaction counters and submit all the required documents to the Evaluator for evaluation and computation of fees.</li>
                                    <li>Proceed to the Cashier when your name is called for the necessary fees and obtain an Official Receipt (OR).</li>
                                    <li>Proceed to the Releasing Counter when your name is called to obtain the Certificate of Registration (CR), plates, RFID/stickers</li>
                                </ol>

                            </div>
                        </div>
                    </div>
                </div>
            @endif

            </div>

        </section>
    </div>


@endsection

@push('scripts')
    <script src="{{ mix('/js/form.js') }}"></script>
@endpush
