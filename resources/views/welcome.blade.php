<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ env('APP_NAME') }}</title>
        <link rel="icon" type="image/png" href="{{ asset('/img/favico.png') }}">
        <!-- Fonts -->

        <link rel="stylesheet" href="{{ asset('/modules/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/modules/ionicons/css/ionicons.min.css') }}">
        <link rel="stylesheet" href="{{ asset('/modules/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css') }}">

        <link rel="stylesheet" href="{{ asset('/css/demo.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/skins/blue.css') }}">

        <!-- Styles -->
        <style>
            html, body {
                background-image: url("{{ asset('/img/hero_low-res.jpg') }}");
                background-size: cover;
                color: #636b6f;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links ">
                    @auth
                        <a class="text-white" href="{{ url('/dashboard') }}">Home</a>
                    @else
                        <a class="text-white" href="{{ route('login') }}">Login</a>

                        @if (Request::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">

                <div class="row justify-content-center">
                    <div class="title m-b-md col-sm-12 col-md-6">
                        <div>
                            <img src="{{ asset('/img/mastlto_large.png') }}">
                        </div>

                        <h1 class="text-white">Welcome to <b>{{ env('APP_NAME') }}</b>. You can now apply online. Anytime. Anywhere.</h1>
                        @auth
                            <a href="{{ route('dashboard') }}" class="btn btn-outline-light">Home</a>
                        @else
                            <a href="{{ route('login') }}" class="btn btn-outline-light">Login</a>
                        @endauth
                    </div>
                </div>


            </div>
        </div>
    </body>
</html>
