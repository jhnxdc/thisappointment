@extends('layouts.app')

@section('content')
    <div class="main-content" id="app">
        <section class="section">
            <h1 class="section-header">
                <div>Applications</div>
            </h1>
            <div class="section-body">

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Pending <div class="badge badge-danger">{{ sizeof($transactions) }}</div></h4>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <form class="row align-items-center">
                                        <div class="col-sm-8 col-md-4">
                                            <label for="exampleFormControlInput1">Date Applied</label>
                                            <div class="input-group date" id="datepicker">
                                                <input type="text" class="form-control" name="date" value="{{ $date }}">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 col-md-1">
                                            <label for="exampleFormControlInput1"></label>
                                            <div class="input-group">
                                                <button type="submit" class="btn btn-primary">View</button>
                                            </div>
                                        </div>

                                        <div class="col-sm-2 col-md-1">
                                            <label for="exampleFormControlInput1"></label>
                                            <div class="input-group">
                                                <a href="{{ route('applications') }}" class="btn btn-danger">Clear</a>
                                            </div>
                                        </div>
                                    </form>
                                    <br>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>Transaction #</th>
                                            <th>Transaction Type</th>
                                            <th>Name</th>
                                            <th>Date applied</th>
                                            <th>Action</th>
                                        </tr>
                                        @foreach($transactions as $transaction)
                                        <tr>
                                            <td class="align-middle">{{ $transaction->id }}</td>
                                            <td class="align-middle">{{ $transaction->transaction_type->name }}</td>

                                            @if($transaction->transaction_type_id == 1)
                                            <td class="align-middle">{{ $transaction->appointment->last_name . ', ' . $transaction->appointment->first_name . ' ' . $transaction->appointment->middle_name }}</td>
                                            @elseif($transaction->transaction_type_id == 2)
                                            <td class="align-middle">{{ $transaction->appointment->name }}</td>
                                            @endif
                                            <td class="align-middle">
                                                {{ $transaction->date_applied }}
                                            </td>
                                            <td class="align-middle"><a href="{{ route('applications.view', ['transaction_id' => $transaction->id]) }}" class="btn btn-action btn-info">Schedule</a></td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>

    <div class="modal fade" id="modal_confirm_reschedule" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Reschedule</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Are you sure you want to reschedule all pending transactions? This cannot be undone.
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <form method="POST" action="{{ route('transaction.reschedule', ['date' => $date]) }}">
                        @csrf
                        <button type="submit" class="btn btn-warning">Reschedule</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('/js/appointments.js') }}"></script>
@endpush
