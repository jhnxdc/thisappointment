@component('mail::message')
@if($transaction->transaction_type_id == 1)
# Application for Driver's License
@elseif($transaction->transaction_type_id == 2)
# Vehicle Registration
@endif

Your appointment is scheduled on {{ \Carbon\Carbon::parse($transaction->date)->format('F d, Y h:00 A') }}. Please come in time.

{!! $body !!}

@if($transaction->transaction_type_id == 1)
@component('mail::button', ['url' => route('print.license') . "?id=" . \Illuminate\Support\Facades\Crypt::encryptString($transaction->id)])
View Form
@endcomponent
@elseif($transaction->transaction_type_id == 2)
@component('mail::button', ['url' => route('print.vehicle') . "?id=" . \Illuminate\Support\Facades\Crypt::encryptString($transaction->id)])
View Form
@endcomponent
@endif



Thanks,<br>
{{ config('app.name') }}
@endcomponent
