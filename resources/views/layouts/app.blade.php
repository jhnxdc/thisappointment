<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
    <title>{{ env('APP_NAME') }}</title>
    <link rel="icon" type="image/png" href="{{ asset('/img/favico.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ asset('/modules/bootstrap/css/bootstrap.min.css')  }}">
    <link rel="stylesheet" href="{{ asset('/modules/ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/modules/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/css/demo.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/skins/blue.css') }}">

    <link rel="stylesheet" href="{{ asset('/modules/bootstrap/css/bootstrap-datepicker.min.css')  }}">
    <link rel="stylesheet" href="{{ asset('/css/bootstrap-datetimepicker-standalone.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css">

    <style>
        body {
            background-image: url("{{ asset('/img/hero_low-res.jpg') }}");
            background-size: cover;
        }
    </style>
</head>

<body>
<div id="app">
    <div class="main-wrapper">
        <div class="navbar-bg"></div>
        <nav class="navbar navbar-expand-lg main-navbar">
            <form class="form-inline mr-auto">
                <ul class="navbar-nav mr-3">
                    <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="ion ion-navicon-round"></i></a></li>
                    <li>
                        <div style="color: white">
                            <div>Philippine Standard Time</div>
                            <div>
                                <span id="pst-time"></span>
                            </div>
                        </div>
                    </li>
                </ul>
            </form>
            <ul class="navbar-nav navbar-right">
                <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg">
                        <i class="ion ion-android-person d-lg-none"></i>
                        <div class="d-sm-none d-lg-inline-block">Hi, {{ Auth::user()->last_name . ' ' . Auth::user()->first_name }}</div></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item has-icon" href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="ion ion-log-out"></i> Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </nav>
        <div class="main-sidebar">
            <aside id="sidebar-wrapper">
                <div class="sidebar-brand">

                    <a href="/">
                        <div>
                            <img src="{{ asset('/img/mastlto.png') }}">
                        </div>
                        {{ env('APP_NAME') }}
                    </a>
                </div>
                <ul class="sidebar-menu">
                    <li class="menu-header">Dashboard</li>
                    @if(Auth::user()->is_admin == 1)
                    <li>
                        <a href="{{ route('dashboard') }}"><i class="ion ion-speedometer"></i><span>Appointments</span></a>
                    </li>
                    @endif

                    @if(Auth::user()->is_admin == 1)
                        <li>
                            <a href="{{ route('applications') }}"><i class="ion ion-android-walk"></i><span>Applications</span></a>
                        </li>
                    @endif

                    @if(Auth::user()->is_admin == 0)
                    <li>
                        <a href="{{ route('user.home') }}"><i class="ion ion-ios-paper"></i><span>Home</span></a>
                    </li>
                    @endif


                    <li class="menu-header"></li>

                    <li>
                        <a data-toggle="modal" data-target="#modal_mission_vision"><span>Mission & Vision</span></a>
                    </li>
                    <li>
                        <a data-toggle="modal" data-target="#modal_historical_background"><span>Historical background</span></a>
                    </li>
                    <li>
                        <a data-toggle="modal" data-target="#modal_law_enforcement"><span>Law Enforcement & Adjusication of Cases</span></a>
                    </li>

                    <li class="menu-header"></li>
                </ul>
            </aside>
        </div>

        @yield('content')

        <footer class="main-footer">
            <div class="footer-left">
                Copyright &copy; 2018
            </div>
            <div class="footer-right"></div>
        </footer>
    </div>
</div>

@include('layouts.modal.mission_vision');
@include('layouts.modal.law_enforcement');
@include('layouts.modal.historical_background');

@stack('scripts')

<script src="{{ asset('/modules/popper.js') }}"></script>
<script src="{{ asset('/modules/tooltip.js') }}"></script>
<script src="{{ asset('/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('/modules/scroll-up-bar/dist/scroll-up-bar.min.js') }}"></script>
<script src="{{ asset('/js/sa-functions.js') }}"></script>

<script src="{{ asset('/js/scripts.js') }}"></script>
<script src="{{ asset('/js/custom.js') }}"></script>


<script type="text/javascript" src="{{ asset('/js/bootstrap-datetimepicker.min.js') }}"></script>
<script>
    $('#datetimepicker_appointment').datetimepicker({
        minDate: moment().add(6, "days").hour(0),
        disabledHours: [0, 1, 2, 3 ,4 ,5 ,6 ,7, 18, 19, 20, 21, 22, 23, 24],
        daysOfWeekDisabled: [6, 0],
        format: 'M/D/YYYY hh:00 A'
    });
</script>

<script type="text/javascript" id="gwt-pst">
    (function(d, eId){
        var js, gjs = d.getElementById(eId);
        js = d.createElement("script"); js.id = "gwt-pst-jsdk";
        js.src = "//gwhs.i.gov.ph/pst/gwtpst.js?"+new Date().getTime();
        gjs.parentNode.insertBefore(js, gjs);
    }(document, "gwt-pst"));
    var gwtpstReady = function(){new gwtpstTime({elementId: 'pst-time',keyboardTrap: true});
    }
</script>


</body>
</html>
