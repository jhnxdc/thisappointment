<div class="modal fade" id="modal_law_enforcement" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Law Enforcement & Adjusication of Cases</h5>
                <ul>
                    <li>
                        Settlement of Admitted Cases (Motor Vehicle [MV] / Plate)
                    </li>
                    <li>
                        Settlement of Admitted Cases (Driver's License [DL])
                    </li>
                    <li>
                        Settlement of Contested Cases (Motor Vehicle)
                    </li>
                    <li>
                        Settlement of Contested Cases (Driver's License)
                    </li>
                    <li>
                        Settlement of Impounded Violation
                    </li>
                    <li>
                        Law Enforcement Certification / Clearance
                    </li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
