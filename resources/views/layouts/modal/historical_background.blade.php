<div class="modal fade" id="modal_historical_background" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Historical Background</h5>
                <p style="text-align: justify;">The concept of land transportation system in the Philippines started when our ancestors invented the means of locomotion with the animals in moving people and goods from place to place.Although the means of land transportation during the early days were not as sophisticated as the modern vehicles of today and the roads not as well constructed, the early Filipinos also observed some laws to govern their mobility.These laws were as informal and simple as specifying which animal could be used for certain purposes butthese showed that our ancestors had already felt the need to regulate the transportation system.</p>

                <p style="text-align: justify;">&nbsp;</p>
                <p style="text-align: justify;">As early as 1910, there were already few motor vehicles seen operating in public highways in Manila and suburbs.</p>
                <p style="text-align: justify;">Better means of transportation were invented and introduced in the country. Gradually,the Filipinos learned to use cars, trucks, jeeps and other types of vehicles.The meansof transportation became better and powerful and the laws governing land travel became more formal and modern.</p>
                <p style="text-align: justify;">On February 6, 1912, Legislative Act No. 2159 was enacted to regulate motor vehicles in the Philippine Islands and to provide for the regulation and licensing of operators. This was the first formal law on land transportation passed by the legislature.This law created an Automobile Section under the Administrative Division of the Bureau of Public Works. The Section was tasked to take charge of motor vehicles and drivers&rsquo; services.</p>
                <p style="text-align: justify;">Later on, Legislative Act 2159 was amended by 2389, 2556, 2587, 2639 and 2925. In 1922, Act No. 3045 was passed into law compiling and incorporating all laws governing motor vehicles.The Automobile Section was upgraded to the Automobile Division under the Bureau of Public Works.</p>
                <p style="text-align: justify;">On January 1, 1933, Act No. 3992 otherwise known as &ldquo;The Revised Motor Vehicle Law&rdquo; was enacted amending Act No. 3045.The Automobile Division was renamed Division of Motor Vehicles.The Chief of the Division was called the Superintendent of Division of Motor Vehicles.Act No. 3992 was amended by Commonwealth Act Numbers 123, 548, 556, 652 and Republic Act Numbers 314, 587, and 2383.</p>
                <p style="text-align: justify;">On June 2, 1945, Department Order No. 4 was issued by the Department of Public Works and Highways reorganizing the Division.This took effect after the liberation of the Philippines from the Japanese invasion.In 1947, Executive Order No. 94 was promulgated reorganizing the different executive departments, bureaus and offices. Under Section 82 of this E.O., the Division of Motor Vehicles was upgraded into the Motor Vehicles Office (MVO) with the category of a Bureau.The Chief of the MVO enjoyed the rights and privileges of a Bureau Director.</p>
                <p style="text-align: justify;">During the fifties and early sixties, our country started undergoing rapid economic development.Industrialization advanced and as a consequence, more and better roads were constructed.The Filipino then realized the need for more and better means of transportation. This need was second only to the need for drugs such as sovaldi. The growth in the number of motor vehicles increased the demand for services that the MVO must render to the public.This necessitated the issuance by the DPWH on June 5, 1961 of Administrative Regulation No. 1 transferring the function of collecting the registration fees from city treasurers to the various vehicle agencies of MVO.</p>
                <p style="text-align: justify;">On June 20, 1964, R.A. 4136, otherwise known as the &ldquo;Land Transportation and Traffic Code&rdquo; was enacted abolishing the Motor Vehicle Office and creating the Land Transportation Commission.This law likewise partially repealed Act No. 3992. The Code provided that the Land Transportation Commission shall &ldquo;control as far as they apply, the registration and operation of motor vehicles and the licensing of owners, dealers, conductors, drivers and similar matters.&rdquo;To effectively carry out its mandate, the Land Transportation Commission established regional offices in various parts of the country.</p>
                <p style="text-align: justify;">On July 23, 1979, Executive Order Number 546 was promulgated creating the Ministry of Transportation and Communications (MOTC).This marked reorganization.The Land Transportation Commission was renamed Bureau of Land Transportation and was absorbed by MOTC.</p>
                <p style="text-align: justify;">On June 2, 1980, Batas Pambansa Bilang 43 was passed providing for the issuance of permanent number plates to owners of motor vehicles and trailers, amending for the purpose section 17 of RA 4136.</p>
                <p style="text-align: justify;">On March 20, 1985, Executive Order 1011 was promulgated.This Executive Order abolished the Board of Transportation and the Bureau of Land Transportation and established the Land Transportation Commission.The defunct BLT and BOT were merged and their powers, functions and responsibilities were transferred to the Land Transportation Commission (LTC) headed by a Chairman, assisted by four Commissioners.The LTC was tasked to perform functions such as registering motor vehicles, licensing of drivers and conductors, franchising of public utility vehicles and enforcing traffic rules and regulations and adjudicating apprehensions. On January 30, 1987, the Land Transportation Commission was abolished and two offices were created, namely:The Land Transportation Office (LTO) and the Land Transportation Franchising and Regulatory Board (LTFRB).The LTO took over the functions of the former BLT and the LTFRB took over the functions of the former BOT. The MOTC was likewise renamed DOTC.</p>
                <p style="text-align: justify;">All these changes took effect with the promulgation of Executive Order No. 125 which was later amended by Executive Orders No. 125-A dated April 13, 1987 and E.O. 226 dated July 25, 1987. Despite the changes in names of Office and all the reorganizations that took effect, its basic functions on land transportation system remain the same.The promotion of safety and comfort in land travel is its continuing commitment. When Executive Orders 125, 125-A and 226 were promulgated, LTO was composed of only thirteen (13) regions.As time went on, additional regions were created, namely: CAR, CARAGA and MIMAROPA.While CAR and CARAGA are attached to the DOTC, they report to LTO Central Office on matters of registration of motor vehicles, issuance of driver's licenses and enforcement of land transportation laws, rules and regulations.</p>
                <p style="text-align: justify;">The LTO is now composed of the following regions:</p>
                <ul>
                    <li>Region I - Aguila Road, Brgy. Sevilla, San Fernando City, La Union</li>
                    <li>Region II - San Gabriel, Tuguegarao, Cagayan</li>
                    <li>Region III - Government Center, Brgy. Maimpis, City of San Fernando, Pampanga</li>
                    <li>Region IVA - J. C. Abadilla Memorial Bldg., Old City Hall Compound, B. Morada Ave., Lipa City Batangas</li>
                    <li>Region IVB - MIMAROPA &ndash; LTO Compd., East Avenue, QC.</li>
                    <li>Region V - Regional Govt. Center Site, Rawis, Legaspi City</li>
                    <li>Region VI - Tabuc-Suba, Jaro, Iloilo City</li>
                    <li>Region VII - Natalio Bacalso Avenue, Cebu City</li>
                    <li>Region VIII &ndash; Old Army Road, Tacloban City</li>
                    <li>Region IX - Veterans Ave., Zamboanga City/Balangasan St., Pagadian</li>
                    <li>Region X - MVIS Compound, Zone 7, Bulua, Cagayan de Oro City</li>
                    <li>Region XI - Quimpo Blvd., Davao City</li>
                    <li>Region XII - ARMM Compound, Cotabato City/No. 79 G. Del Pilar St., Koronadal City, South Cotabato</li>
                    <li>National Capital Region (NCR) &ndash; #20 G. Araneta Avenue, Brgy. Sto Domingo, Q.C.</li>
                    <li>Cordillera Administrative Region (CAR) - Engineer&rsquo;s Hill, Baguio City/2nd Flr., Post Office Loop, Session Road, Baguio City</li>
                    <li>CARAGA - J. Rosales Avenue, Butuan City</li>
                </ul>
                <p style="text-align: justify;">The volume of transactions at LTO has grown so fast without increasing the manpower, hence to meet the public&rsquo;s demand, LTO introduced the computerization of its transactions sometime in 1998.To date, almost all LTO Offices are computerized except those with manpower, telecommunications and security issues.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
