<div class="modal fade" id="modal_mission_vision" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Mission</h5>
                <p>
                    Rationalize the land transportation services and facilities and to effectively implement the various transportation laws, rules and regulations. It is the responsibility of those involved in the public service to be more vigilant in their part in the over-all development scheme of the national leadership. Hence, promotion of safety and comfort in land travel is a continuing commitment of the LTO.
                </p>

                <br>
                <br>

                <h5>Vision</h5>
                <p>
                    A front line government agency showcasing fast and efficient public service for a progressive land transport sector.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
