@extends('layouts.app')

@section('content')
    <div class="main-content" id="app">
        <section class="section">
            <h1 class="section-header">
                <div>Application for Driver's License</div>
            </h1>
            <div class="section-body">

                @if($errors->has('date'))
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="alert alert-danger" role="alert">
                                No more slots left on the selected date.
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Select one</h4>
                            </div>
                            <div class="card-body">

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="first_name">First Name</label>
                                                    <input class="form-control" name="first_name" id="first_name" value="{{ $drivers_license->first_name }} " readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="last_name">Last Name</label>
                                                    <input class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" id="last_name" value="{{ $drivers_license->last_name }}" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="middle_name">Middle Name</label>
                                                    <input class="form-control" name="middle_name" id="middle_name" value="{{ $drivers_license->middle_name }}" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="address">Present Address (No., Street, City/Municipality, Province)</label>
                                                    <input class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" id="address" value="{{ $drivers_license->address }}" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="tin">TIN</label>
                                                    <input class="form-control" name="tin" id="tin" value="{{ $drivers_license->tin }}" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="tel_no">Tel no. / CP no.</label>
                                                    <input class="form-control" name="tel_no" id="tel_no" value="{{ $drivers_license->tel_no }}" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="nationality">Nationality</label>
                                                    <input class="form-control {{ $errors->has('nationality') ? ' is-invalid' : '' }}" name="nationality" id="nationality" value="{{ $drivers_license->nationality }}" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Gender</label>
                                                    <div>
                                                        <div class="form-check form-check-inline">
                                                            <input {{ $drivers_license->gender == 'Male' ? 'checked' : '' }} class="form-check-input" type="radio" name="gender" id="gender" value="Male" checked="checked" disabled>
                                                            <label class="form-check-label" for="inlineRadio1">Male</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input {{ $drivers_license->gender == 'Female' ? 'checked' : '' }} class="form-check-input" type="radio" name="gender" id="gender" value="Female" disabled>
                                                            <label class="form-check-label" for="inlineRadio2">Female</label>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-sm-12 col-md-4">
                                                            <label for="exampleFormControlInput1">Birth date</label>
                                                            <div class="input-group date" data-provide="datepicker" id="datepicker">
                                                                <input type="text" class="form-control {{ $errors->has('dob') ? ' is-invalid' : '' }}" name="dob" value="{{ $drivers_license->dob }}" readonly>
                                                                <div class="input-group-addon">
                                                                    <span class="glyphicon glyphicon-th"></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Weight (kg)</label>
                                                    <input type="number" min="0" class="form-control {{ $errors->has('weight') ? ' is-invalid' : '' }}" name="weight" id="weight" value="{{ $drivers_license->weight }}" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Height (cm)</label>
                                                    <input type="number" min="0" class="form-control {{ $errors->has('height') ? ' is-invalid' : '' }}" name="height" id="height" value="{{ $drivers_license->height }}" readonly>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Type of Application (TOA)</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-check">
                                                    <input {{ $drivers_license->toa == 'New' ? 'checked' : '' }} class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="New" checked="checked" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        New
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->toa == 'Delinquent/Dormant license' ? 'checked' : '' }} class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Delinquent/Dormant license" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Delinquent/Dormant license
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->toa == 'Prof to Non-Prof' ? 'checked' : '' }} class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Prof to Non-Prof" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Prof to Non-Prof
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->toa == 'Non-Prof to Prof' ? 'checked' : '' }} class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Non-Prof to Prof" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Non-Prof to Prof
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->toa == 'Foreign Lic. Conversion' ? 'checked' : '' }} class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Foreign Lic. Conversion" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Foreign Lic. Conversion
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->toa == 'Renewal' ? 'checked' : '' }} class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Renewal" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Renewal
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->toa == 'Additional Restriction Code' ? 'checked' : '' }} class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Additional Restriction Code" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Additional Restriction Code
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->toa == 'Duplicate' ? 'checked' : '' }} class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Duplicate" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Duplicate
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->toa == 'Revision of Records' ? 'checked' : '' }} class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Revision of Records" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Revision of Records
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->toa == 'Change Address' ? 'checked' : '' }} class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Change Address" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Change Address
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->toa == 'Change Civil Status' ? 'checked' : '' }} class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Change Civil Status" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Change Civil Status
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->toa == 'Change Name' ? 'checked' : '' }} class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Change Name" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Change Name
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->toa == 'Change Date of Birth' ? 'checked' : '' }} class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Change Date of Birth" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Change Date of Birth
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->toa == 'Others' ? 'checked' : '' }} class="form-check-input" type="radio" name="toa" id="exampleRadios2" value="Others" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Others
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Type of License Applied for (TLA)</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-check">
                                                    <input {{ $drivers_license->tla == 'Student Permit' ? 'checked' : '' }} class="form-check-input" type="radio" name="tla" id="exampleRadios2" value="Student Permit" checked="checked" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Student Permit
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->tla == 'Non-professional' ? 'checked' : '' }} class="form-check-input" type="radio" name="tla" id="exampleRadios2" value="Non-professional" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Non-professional
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->tla == 'Professional' ? 'checked' : '' }} class="form-check-input" type="radio" name="tla" id="exampleRadios2" value="Professional" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Professional
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->tla == 'Conductor' ? 'checked' : '' }} class="form-check-input" type="radio" name="tla" id="exampleRadios2" value="Conductor" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Conductor
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Driving skill acquired or will be acquired thru (DSA)</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-check">
                                                    <input {{ $drivers_license->dsa == 'Driving School' ? 'checked' : '' }} class="form-check-input" type="radio" name="dsa" id="exampleRadios2" value="Driving School" checked="checked" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Driving School
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->dsa == 'Licensed Private Person' ? 'checked' : '' }} class="form-check-input" type="radio" name="dsa" id="exampleRadios2" value="Licensed Private Person" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Licensed Private Person
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Educational Attainment (EA)</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-check">
                                                    <input {{ $drivers_license->ea == 'Informal Schooling' ? 'checked' : '' }} class="form-check-input" type="radio" name="ea" id="exampleRadios2" value="Informal Schooling" checked="checked" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Informal Schooling
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->ea == 'Elementary' ? 'checked' : '' }} class="form-check-input" type="radio" name="ea" id="exampleRadios2" value="Elementary" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Elementary
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->ea == 'High School' ? 'checked' : '' }} class="form-check-input" type="radio" name="ea" id="exampleRadios2" value="High School" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        High School
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->ea == 'Vocational' ? 'checked' : '' }} class="form-check-input" type="radio" name="ea" id="exampleRadios2" value="Vocational" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Vocational
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->ea == 'College' ? 'checked' : '' }} class="form-check-input" type="radio" name="ea" id="exampleRadios2" value="College" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        College
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input {{ $drivers_license->ea == 'Post Graduate' ? 'checked' : '' }} class="form-check-input" type="radio" name="ea" id="exampleRadios2" value="Post Graduate" disabled>
                                                    <label class="form-check-label" for="exampleRadios2">
                                                        Post Graduate
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4></h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Blood type</label>
                                                    <input class="form-control" name="blood_type" id="blood_type" value="{{ $drivers_license->blood_type }}" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Organ Donor</label>
                                                    <div>
                                                        <div class="form-check form-check-inline">
                                                            <input {{ $drivers_license->organ_donor == 'Yes' ? 'checked' : '' }} class="form-check-input" type="radio" name="organ_donor" id="inlineRadio1" value="Yes" disabled>
                                                            <label class="form-check-label" for="inlineRadio1">Yes</label>
                                                        </div>
                                                        <div class="form-check form-check-inline">
                                                            <input {{ $drivers_license->organ_donor == 'No' ? 'checked' : '' }} class="form-check-input" type="radio" name="organ_donor" id="inlineRadio2" value="No" disabled>
                                                            <label class="form-check-label" for="inlineRadio2">No</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Civil Status (SC)</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->cs == 'Single' ? 'checked' : '' }} class="form-check-input" type="radio" name="cs" id="inlineRadio1" value="Single" checked="checked" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Single</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->cs == 'Married' ? 'checked' : '' }} class="form-check-input" type="radio" name="cs" id="inlineRadio1" value="Married" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Married</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->cs == 'Widower' ? 'checked' : '' }} class="form-check-input" type="radio" name="cs" id="inlineRadio1" value="Widower" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Widower</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->cs == 'Separated' ? 'checked' : '' }} class="form-check-input" type="radio" name="cs" id="inlineRadio1" value="Separated" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Separated</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Hair</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->hair == 'Black' ? 'checked' : '' }} class="form-check-input" type="radio" name="hair" id="inlineRadio1" value="Black" checked="checked" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Black</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->hair == 'Brown' ? 'checked' : '' }} class="form-check-input" type="radio" name="hair" id="inlineRadio1" value="Brown" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Brown</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->hair == 'Blonde' ? 'checked' : '' }} class="form-check-input" type="radio" name="hair" id="inlineRadio1" value="Blonde" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Blonde</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->hair == 'Gray' ? 'checked' : '' }} class="form-check-input" type="radio" name="hair" id="inlineRadio1" value="Gray" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Gray</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Eyes</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->eyes == 'Black' ? 'checked' : '' }} class="form-check-input" type="radio" name="eyes" id="inlineRadio1" value="Black" checked="checked" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Black</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->eyes == 'Brown' ? 'checked' : '' }} class="form-check-input" type="radio" name="eyes" id="inlineRadio1" value="Brown" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Brown</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->eyes == 'Gray' ? 'checked' : '' }} class="form-check-input" type="radio" name="eyes" id="inlineRadio1" value="Gray" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Gray</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Built</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->built == 'Light' ? 'checked' : '' }} class="form-check-input" type="radio" name="built" id="inlineRadio1" value="Light" checked="checked" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Light</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->built == 'Medium' ? 'checked' : '' }} class="form-check-input" type="radio" name="built" id="inlineRadio1" value="Medium" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Medium</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->built == 'Heavy' ? 'checked' : '' }} class="form-check-input" type="radio" name="built" id="inlineRadio1" value="Heavy" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Heavy</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4>Complexion</h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->complexion == 'Light' ? 'checked' : '' }} class="form-check-input" type="radio" name="complexion" id="inlineRadio1" value="Light" checked="checked" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Light</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->complexion == 'Fair' ? 'checked' : '' }} class="form-check-input" type="radio" name="complexion" id="inlineRadio1" value="Fair" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Fair</label>
                                                </div>

                                                <div class="form-check form-check-inline">
                                                    <input {{ $drivers_license->complexion == 'Dark' ? 'checked' : '' }} class="form-check-input" type="radio" name="complexion" id="inlineRadio1" value="Dark" disabled>
                                                    <label class="form-check-label" for="inlineRadio1">Dark</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-header">
                                                <h4></h4>
                                            </div>
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Birth place</label>
                                                    <input class="form-control {{ $errors->has('birth_place') ? ' is-invalid' : '' }}" name="birth_place" id="birth_place" value="{{ $drivers_license->birth_place }}" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Father's name</label>
                                                    <input class="form-control {{ $errors->has('fathers_name') ? ' is-invalid' : '' }}" name="fathers_name" id="exampleFormControlInput1" value="{{ $drivers_license->fathers_name }}" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Mother's name</label>
                                                    <input class="form-control {{ $errors->has('mothers_name') ? ' is-invalid' : '' }}" name="mothers_name" id="exampleFormControlInput1" value="{{ $drivers_license->mothers_name }}" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Spouse name</label>
                                                    <input class="form-control {{ $errors->has('spouse_name') ? ' is-invalid' : '' }}" name="spouse_name" id="exampleFormControlInput1" value="{{ $drivers_license->spouse_name }}" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Employer's Business Name</label>
                                                    <input class="form-control {{ $errors->has('emp_bus_name') ? ' is-invalid' : '' }}" name="emp_bus_name" id="exampleFormControlInput1" value="{{ $drivers_license->emp_bus_name }}" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Tel no.</label>
                                                    <input class="form-control {{ $errors->has('emp_tel_no') ? ' is-invalid' : '' }}" name="emp_tel_no" id="exampleFormControlInput1" value="{{ $drivers_license->emp_tel_no }}" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Employer's business address</label>
                                                    <input class="form-control {{ $errors->has('emp_bus_add') ? ' is-invalid' : '' }}" name="emp_bus_add" id="exampleFormControlInput1" value="{{ $drivers_license->emp_bus_add }}" readonly>
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleFormControlInput1">Previous Name</label>
                                                    <input class="form-control {{ $errors->has('prev_name') ? ' is-invalid' : '' }}" name="prev_name" id="exampleFormControlInput1" value="{{ $drivers_license->prev_name }}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                @if (!$transaction->is_done)
                                    <form method="post" action="{{ route('applications.schedule', ['transaction_id' => $drivers_license->transaction_id]) }}">
                                        @csrf

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="card">
                                                    <div class="card-header">
                                                        <h4>Select a date</h4>
                                                    </div>
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-sm-7 col-md-4">
                                                                <div class="input-group date">
                                                                    <input type='text' class="form-control" name="date_applied" id='datetimepicker_appointment'/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <label for="exampleFormControlTextarea1">Email body</label>
                                                                    <textarea class="form-control" name="email_body" id="exampleFormControlTextarea1" rows="10">
Please be on {BRANCH} thirty minutes before your scheduled appointment

Please make sure you have prepared all requirements

Be ready with the original and photocopies of the documents when you appear for personal appearance. Bring the old Driver's License

Kindly print your application form with the exact date and schedule in A4 size or long size bond paper. You must have a printed application form to show and submit to the chosen site.

*You can print this email for reference.</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-3">
                                                                <button href="#" class="btn btn-primary">Schedule</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                @endif

                                <br>

                                @if (!$transaction->is_done)
                                    <form method="post" action="{{ route('transaction.process', ['transaction_id' => $drivers_license->transaction_id]) }}">
                                        @csrf
                                        <button href="#" class="btn btn-danger">Deny Application</button>
                                    </form>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


@endsection

@push('scripts')
    <script src="{{ mix('/js/app.js') }}"></script>
@endpush
