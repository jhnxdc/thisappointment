@extends('layouts.app')

@section('content')
    <div class="main-content" id="app">
        <section class="section">
            <h1 class="section-header">
                <div>Motor Vehicle Inspection Form</div>
            </h1>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>OWNERSHIP AND DOCUMENTATION</h4>
                            </div>
                            <div class="card-body">

                                <div class="form-group">
                                    <label for="name">Owner's complete name and address</label>
                                    <input class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" id="name" value="{{ $vehicle_registration->name }} " readonly>
                                </div>

                                <div class="form-group">
                                    <label for="from">Acquired from (complete name and address)</label>
                                    <input class="form-control {{ $errors->has('from') ? ' is-invalid' : '' }}" name="from" id="from" value="{{ $vehicle_registration->from }} " readonly>
                                </div>

                                <div class="form-group">
                                    <label for="encumbrance">Encumbrance (Company name and address)</label>
                                    <input class="form-control {{ $errors->has('encumbrance') ? ' is-invalid' : '' }}" name="encumbrance" id="encumbrance" value="{{ $vehicle_registration->encumbrance }} " readonly>
                                </div>

                                <div class="form-group">
                                    <label for="exampleFormControlInput1">Kind of vehicle</label>
                                    <div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="kov" id="kov" value="New" checked="checked" {{ $vehicle_registration->kov == 'New' ? 'checked' : '' }} disabled>
                                            <label class="form-check-label" for="inlineRadio1">New</label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="kov" id="kov" value="2nd hand" {{ $vehicle_registration->kov == '2nd hand' ? 'checked' : '' }} disabled>
                                            <label class="form-check-label" for="inlineRadio1">2nd hand</label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="kov" id="kov" value="Rebuilt" {{ $vehicle_registration->kov == 'Rebuilt' ? 'checked' : '' }} disabled>
                                            <label class="form-check-label" for="inlineRadio1">Rebuilt</label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="kov" id="kov" value="Car" {{ $vehicle_registration->kov == 'Car' ? 'checked' : '' }} disabled>
                                            <label class="form-check-label" for="inlineRadio1">Car</label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="kov" id="kov" value="Truck" {{ $vehicle_registration->kov == 'Truck' ? 'checked' : '' }} disabled>
                                            <label class="form-check-label" for="inlineRadio1">Truck</label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="kov" id="kov" value="Hire" {{ $vehicle_registration->kov == 'Hire' ? 'checked' : '' }} disabled>
                                            <label class="form-check-label" for="inlineRadio1">Hire</label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="kov" id="kov" value="MC" {{ $vehicle_registration->kov == 'MC' ? 'checked' : '' }} disabled>
                                            <label class="form-check-label" for="inlineRadio1">MC</label>
                                        </div>

                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="kov" id="kov" value="TC" {{ $vehicle_registration->kov == 'TC' ? 'checked' : '' }} disabled>
                                            <label class="form-check-label" for="inlineRadio1">TC</label>
                                        </div>
                                    </div>
                                </div>

                                @if (!$transaction->is_done)
                                <form method="post" action="{{ route('transaction.process', ['transaction_id' => $vehicle_registration->transaction_id]) }}">
                                    @csrf
                                    <button href="#" class="btn btn-primary">Process</button>
                                </form>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>


@endsection

@push('scripts')
    <script src="{{ mix('/js/app.js') }}"></script>
@endpush
