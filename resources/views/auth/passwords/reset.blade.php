<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" name="viewport">
    <title>Reset &mdash; {{env('APP_NAME')}}</title>
    <link rel="icon" type="image/png" href="{{ asset('/img/favico.png') }}">

    <link rel="stylesheet" href="{{ asset('/modules/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/modules/ionicons/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/modules/fontawesome/web-fonts-with-css/css/fontawesome-all.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/css/demo.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/skins/blue.css') }}">


    <style>
        html {
            height: 100%;
        }
        body {
            background-image: url("{{ asset('/img/hero_low-res.jpg') }}");
            background-size: cover;
        }
    </style>
</head>

<body>
<div id="app">
    <section class="section">
        <div class="container mt-5">
            <div class="row">
                <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                    <div class="login-brand">
                        <a class="text-white" style="text-decoration : none" href="{{ route('super_home') }}">
                            <div>
                                <img src="{{ asset('/img/mastlto.png') }}">
                            </div>
                            {{ env('APP_NAME') }}
                        </a>
                    </div>

                    <div class="card card-primary">
                        <div class="card-header"><h4>Reset Password</h4></div>

                        <div class="card-body">
                            <p class="text-muted">We will send a link to reset your password</p>
                            <form method="POST" action="{{ route('password.update') }}">
                                @csrf
                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" tabindex="1" required autofocus>
                                    @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="password">New Password</label>
                                    <input id="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" data-indicator="pwindicator" name="password" tabindex="2" required>
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="password-confirm">Confirm Password</label>
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" tabindex="2" required>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-block" tabindex="4">
                                        Reset Password
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="simple-footer">
                        Copyright &copy; 2018
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<script src="{{ asset('/modules/jquery.min.js') }}"></script>
<script src="{{ asset('/modules/popper.js') }}"></script>
<script src="{{ asset('/modules/tooltip.js') }}"></script>
<script src="{{ asset('/modules/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/modules/nicescroll/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('/modules/moment.min.js') }}"></script>
<script src="{{ asset('/modules/scroll-up-bar/scroll-up-bar.min.js') }}"></script>
<script src="{{ asset('/js/sa-functions.js') }}"></script>

<script src="{{ asset('/js/scripts.js') }}"></script>
<script src="{{ asset('/js/custom.js') }}"></script>
</body>
</html>
