require('./bootstrap');

window.Vue = require('vue');
window.datepicker = require('bootstrap-datepicker');
window.moment = require('moment');

new Vue({
    el: '#app',
    mounted: function() {
        $('#datepicker').datepicker({
            todayBtn: true,
            todayHighlight: true,
            format: 'yyyy-m-dd'
        });

//        $('#datepicker').datepicker('update', moment().format());
    }
});
