require('./bootstrap');

window.Vue = require('vue');
window.datepicker = require('bootstrap-datepicker');
window.moment = require('moment');

new Vue({
    el: '#app',
    created() {
        $('#datepicker').datepicker();
    }
});
