<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('super_home');

Auth::routes();

Route::middleware(['admin'])->group(function () {
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');

    Route::get('/applications', 'TransactionController@applications')->name('applications');
    Route::get('/applications/view/{transaction_id}', 'TransactionController@viewApplication')->name('applications.view');
    Route::post('/applications/schedule/{transaction_id}', 'TransactionController@scheduleApplication')->name('applications.schedule');

    Route::post('/transactions/reschedule/{date}', 'TransactionController@reschedule')->name('transaction.reschedule');

    Route::get('/transactions/view/{transaction_id}', 'TransactionController@viewTransaction')->name('transaction.view');

    Route::post('/transactions/process/{transaction_id}', 'TransactionController@process')->name('transaction.process');
});

Route::middleware(['auth'])->group(function () {
    Route::get('/home', 'UserPagesController@home')->name('user.home');

    Route::get('/transactions/license', 'UserPagesController@license')->name('user.license');
    Route::post('/transactions/license', 'TransactionController@newLicenseTransaction')->name('transaction.license');

    Route::get('/transactions/vehicle', 'UserPagesController@vehicle')->name('user.vehicle');
    Route::post('/transactions/vehicle', 'TransactionController@vehicleRegistrationTransaction')->name('transaction.vehicle');

    Route::get('success', 'TransactionController@success')->name('success');

    Route::get('/profile', 'UserPagesController@profile')->name('profile');
    Route::post('/profile', 'UserPagesController@profileUpdate')->name('profile');
    Route::post('/profile/email', 'UserPagesController@profileEmail')->name('profile.email');
    Route::post('/profile/password', 'UserPagesController@profilePassword')->name('profile.password');
});

Route::get('/mailable', function () {
    $transaction = \App\Transaction::find(3);

    return new App\Mail\TransactionScheduled($transaction);
});

Route::get('/print/vehicle', 'TransactionController@printVehicle')->name('print.vehicle');
Route::get('/print/license', 'TransactionController@printLicense')->name('print.license');
