<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleRegistration extends Model
{
    protected $fillable = [
        'transaction_id',
        'name',
        'from',
        'encumbrance',
        'kov',
    ];
}
