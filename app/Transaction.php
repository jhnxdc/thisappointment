<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Transaction extends Model
{
    public function appointment() {
        if ($this->transaction_type_id == 1) {
            return $this->hasOne('App\DriversLicense', 'transaction_id', 'id');
        } else if ($this->transaction_type_id == 2) {
            return $this->hasOne('App\VehicleRegistration', 'transaction_id', 'id');
        }

        return null;
    }

    public function transaction_type() {
        return $this->hasOne('App\TransactionType', 'id', 'transaction_type_id');
    }


    public static function findAvailableDate($transaction) {
        $limit = 1000;

        $expected_date = Carbon::now();
        $expected_date->addDay();

        $is_date_available = false;

        while (!$is_date_available) {
            $transactions_count = Transaction::whereDate('date', $expected_date)->count();

            if ($transactions_count < $limit) {
                $transaction_check = Transaction::whereDate('date', $expected_date)
                                        ->where('user_id', '=', Auth::user()->id)
                                        ->count();
                if ($transaction_check == 0) {
                    $transaction->date = $expected_date->format('Y/m/d');
                    $transaction->is_morning = $transactions_count < ($limit / 2);

                    $is_date_available = true;
                } else {
                    $expected_date->addDay();
                }
            } else {
                $expected_date->addDay();
            }
        }

        return $expected_date;
    }

    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
