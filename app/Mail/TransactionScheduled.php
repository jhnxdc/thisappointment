<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TransactionScheduled extends Mailable
{
    use Queueable, SerializesModels;

    public $transaction;
    public $body;

    /**
     * TransactionScheduled constructor.
     * @param $transaction
     */
    public function __construct($transaction, $body)
    {
        $this->transaction = $transaction;
        $this->body = $body;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        /*return (new MailMessage)
            ->subject(Lang::getFromJson('LTO Online Application'))
            ->line(Lang::getFromJson('Your appointment is scheduled on' . Carbon::parse($this->transaction->date)->format('F d, Y') . ' in the ' . $this->transaction ? 'morning.' : 'afternoon.'))
            ->action(Lang::getFromJson('View Requirements'), url(config('app.url').route('print.vehicle')));
        */

        return $this->markdown('emails.transaction_scheduled');
    }
}
