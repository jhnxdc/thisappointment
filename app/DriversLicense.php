<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriversLicense extends Model
{
    protected $fillable = [
        'transaction_id',
        'first_name',
        'last_name',
        'middle_name',
        'address',
        'tin',
        'tel_no',
        'nationality',
        'gender',
        'dob',
        'weight',
        'height',

        'toa',

        'tla',

        'dsa',

        'ea',

        'blood_type',
        'organ_donor',

        'cs',

        'hair',

        'eyes',

        'built',

        'complexion',

        'birth_place',
        'fathers_name',
        'mothers_name',
        'spouse_name',
        'emp_bus_name',
        'emp_tel_no',
        'emp_bus_add',
        'prev_name',
    ];
}
