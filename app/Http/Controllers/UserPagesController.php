<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserPagesController extends Controller
{
    public function home() {
        return view('user.home');
    }

    public function license() {
        return view('user.license');
    }

    public function vehicle() {
        return view('user.vehicle');
    }

    public function profile() {
        return view('user.profile');
    }

    public function profileUpdate(Request $request) {
        $request->validate([
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'middle_name' => 'nullable|max:255',
        ]);

        $user = Auth::user();

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->middle_name = $request->middle_name;
        $user->save();

        return redirect()->back();
    }

    public function profileEmail(Request $request) {
        $request->validate([
            'email' => 'required|unique:users|max:60',
        ]);

        $user = Auth::user();

        $user->email = $request->email;
        $user->save();

        return redirect()->back();
    }

    public function profilePassword(Request $request) {
        $request->validate([
            'password' => 'min:6|confirmed'
        ]);

        $user = Auth::user();
        $user->password = bcrypt($request->input('password'));

        $user->save();

        return redirect()->back();
    }
}
