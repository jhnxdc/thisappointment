<?php

namespace App\Http\Controllers;

use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $date = Input::get('date', null);
        $search = Input::get('search', null);

        if($date == null) {
            $today = Carbon::now();
        } else {
            $today = Carbon::parse($date);
        }

        $transactions_pending = Transaction::query();

        $transactions_pending->whereDate('date', $today)
                                ->orderBy('updated_at', 'asc')
                                ->where('is_done', false);

        $transactions_pending = $transactions_pending->get();

        if ($search != null) {
            //Drivers License

            $transactions_pending_search = Transaction::query();
            $transactions_pending_search->whereDate('date', $today)
                ->orderBy('updated_at', 'asc')
                ->where('is_done', false);

            $transactions_pending_search->leftJoin('drivers_licenses', 'transactions.id', 'drivers_licenses.transaction_id');

            $transactions_pending_search->where(function ($query) {
                $query->orWhere('drivers_licenses.first_name', 'like', '%' . Input::get('search', null) . '%');
                $query->orWhere('drivers_licenses.last_name', 'like', '%' . Input::get('search', null) . '%');
                $query->orWhere('drivers_licenses.transaction_id', '=', Input::get('search', null));
            });
            $transactions_pending_search->select('transactions.*');

            $transactions_pending_search = $transactions_pending_search->get();


            //Vehicle Registration

            $transactions_pending_search_v = Transaction::query();
            $transactions_pending_search_v->whereDate('date', $today)
                ->orderBy('updated_at', 'asc')
                ->where('is_done', false);
            $transactions_pending_search_v->leftJoin('vehicle_registrations', 'transactions.id', 'vehicle_registrations.transaction_id');

            $transactions_pending_search_v->where(function ($query) {
                $query->orWhere('vehicle_registrations.name', 'like', '%' . Input::get('search', null) . '%');
                $query->orWhere('vehicle_registrations.transaction_id', '=', Input::get('search', null));
            });
            $transactions_pending_search_v->select('transactions.*');

            $transactions_pending_search_v = $transactions_pending_search_v->get();

            $result_array = array();
            foreach ($transactions_pending_search as $array) {
                $result_array[] = $array;
            }
            foreach ($transactions_pending_search_v as $array) {
                $result_array[] = $array;
            }

            $transactions_pending = $result_array;
        }

        $transactions_processed = Transaction::query();
        $transactions_processed->whereDate('date', $today)
                                    ->orderBy('updated_at', 'asc')
                                    ->where('is_done', true)
                                    ->get();

        $transactions_processed = $transactions_processed->get();

        if ($search != null) {
            //Drivers License

            $transactions_pending_search = Transaction::query();
            $transactions_pending_search->whereDate('date', $today)
                ->orderBy('updated_at', 'asc')
                ->where('is_done', true);

            $transactions_pending_search->leftJoin('drivers_licenses', 'transactions.id', 'drivers_licenses.transaction_id');

            $transactions_pending_search->where(function ($query) {
                $query->orWhere('drivers_licenses.first_name', 'like', '%' . Input::get('search', null) . '%');
                $query->orWhere('drivers_licenses.last_name', 'like', '%' . Input::get('search', null) . '%');
                $query->orWhere('drivers_licenses.transaction_id', '=', Input::get('search', null));
            });
            $transactions_pending_search->select('transactions.*');

            $transactions_pending_search = $transactions_pending_search->get();


            //Vehicle Registration

            $transactions_pending_search_v = Transaction::query();
            $transactions_pending_search_v->whereDate('date', $today)
                ->orderBy('updated_at', 'asc')
                ->where('is_done', true);
            $transactions_pending_search_v->leftJoin('vehicle_registrations', 'transactions.id', 'vehicle_registrations.transaction_id');

            $transactions_pending_search_v->where(function ($query) {
                $query->orWhere('vehicle_registrations.name', 'like', '%' . Input::get('search', null) . '%');
                $query->orWhere('vehicle_registrations.transaction_id', '=', Input::get('search', null));
            });
            $transactions_pending_search_v->select('transactions.*');

            $transactions_pending_search_v = $transactions_pending_search_v->get();

            $result_array = array();
            foreach ($transactions_pending_search as $array) {
                $result_array[] = $array;
            }
            foreach ($transactions_pending_search_v as $array) {
                $result_array[] = $array;
            }

            $transactions_processed = $result_array;
        }

        return view('dashboard', ['transactions_pending' => $transactions_pending, 'transactions_processed' => $transactions_processed, 'date' => $today->format('Y-m-d')]);
    }
}
