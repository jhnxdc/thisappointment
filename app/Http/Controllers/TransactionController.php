<?php

namespace App\Http\Controllers;

use App\DriversLicense;
use App\Mail\TransactionScheduled;
use App\Transaction;
use App\VehicleRegistration;
use Carbon\Carbon;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class TransactionController extends Controller
{
    public function newLicenseTransaction(Request $request) {
        $request->validate([
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'middle_name' => 'nullable|max:255',
            'address' => 'required|max:255',
            'tin' => 'nullable|max:255',
            'tel_no' => 'nullable|max:255',
            'nationality' => 'required|max:255',
            'gender' => 'required|max:255',
            'dob' => 'required|max:255',
            'weight' => 'nullable|integer',
            'height' => 'nullable|integer',

            'organ_donor' => 'nullable|max:255',

            'birth_place' => 'required|max:255',
            'fathers_name' => 'nullable|max:255',
            'mothers_name' => 'nullable|max:255',
            'spouse_name' => 'nullable|max:255',
            'emp_bus_name' => 'nullable|max:255',
            'emp_tel_no' => 'nullable|max:255',
            'emp_bus_add' => 'nullable|max:255',
            'prev_name' => 'nullable|max:255',

        ]);

        $transaction = new Transaction();
        $transaction->transaction_type_id = 1;
        $transaction->user_id = Auth::user()->id;
        //Transaction::findAvailableDate($transaction);

        $transaction->date_applied = new Carbon();

        $transaction->save();

        $drivers_license = DriversLicense::create(array_merge($request->all(), ['transaction_id' => $transaction->id]));

    //    Mail::to($transaction->user)->send(new TransactionScheduled($transaction));

        return redirect('success')->with('transaction_id', $transaction->id);
    }


    public function vehicleRegistrationTransaction(Request $request) {
        $request->validate([
            'name' => 'required|max:255',
            'kov' => 'required|max:255',
            'from' => 'nullable|max:255',
            'encumbrance' => 'nullable|max:255',
        ]);

        $transaction = new Transaction();
        $transaction->transaction_type_id = 2;
        $transaction->user_id = Auth::user()->id;
        //Transaction::findAvailableDate($transaction);
        $transaction->date_applied = new Carbon();

        $transaction->save();

        $vehicle_registration = VehicleRegistration::create(array_merge($request->all(), ['transaction_id' => $transaction->id]));

    //    Mail::to($transaction->user)->send(new TransactionScheduled($transaction));

        return redirect('success')->with('transaction_id', $transaction->id);
    }

    public function success() {
        $transaction_id = session('transaction_id');

        //$transaction_id = 1;

        if (!isset($transaction_id))
            return redirect('home');

        $transaction = Transaction::find($transaction_id);
        $transaction->appointment;

        return view('user.success', ['transaction' => $transaction]);
    }

    public function reschedule($date) {
        if ($date == null)
            return redirect()->back();

        $date = Carbon::parse($date);

        $transactions = Transaction::whereDate('date', $date)
                                    ->where('is_done', false)
                                    ->update(['date' => $date->addDay(), 'updated_at' => Carbon::now()]);

        return redirect()->back();
    }

    public function viewTransaction($transaction_id) {
        $transaction = Transaction::where('id', '=', $transaction_id)->first();

        if ($transaction == null)
            return redirect()->back();

        $appointment = $transaction->appointment;

        if ($transaction->transaction_type_id == 1) {
            return view('view_license', ['drivers_license' => $appointment, 'transaction' => $transaction]);
        } else if ($transaction->transaction_type_id == 2) {
            return view('view_vehicle', ['vehicle_registration' => $appointment, 'transaction' => $transaction]);
        } else {

        }

        return view('view_license', ['drivers_license' => $appointment, 'transaction' => $transaction]);
    }

    public function process($transaction_id) {
        $transaction = Transaction::where('id', '=', $transaction_id)->first();

        if ($transaction == null)
            return redirect()->back();

        $transaction->is_done = true;

        $transaction->save();

        return redirect()->route('dashboard');
    }

    public function printVehicle() {
        $id = Input::get('id');

        try {
            $decrypt_id = Crypt::decryptString($id);
            $transaction = Transaction::where('id', '=', $decrypt_id)->first();
            if ($transaction == null)
                return "Invalid request";

            $appointment = $transaction->appointment;

            return view('user.print.vehicle', ["vehicle_registration" => $appointment, "transaction" => $transaction]);
        } catch (DecryptException $e) {
            return "Invalid request";
        }
    }

    public function printLicense() {
        $id = Input::get('id');

        try {
            $decrypt_id = Crypt::decryptString($id);
            $transaction = Transaction::where('id', '=', $decrypt_id)->first();
            if ($transaction == null)
                return "Invalid request";

            $appointment = $transaction->appointment;

            return view('user.print.license', ["drivers_license" => $appointment, "transaction" => $transaction]);
        } catch (DecryptException $e) {
            return "Invalid request";
        }

        return view('user.print.license');
    }

    public function applications() {
        $date = Input::get('date', null);

        if($date == null) {
            $today = Carbon::now();
        } else {
            $today = Carbon::parse($date);
        }

        $transactions = Transaction::query();

        $transactions->where('date', '=', null);

        if ($date != null) {
            $carbon_date = new Carbon($date);

            $transactions->whereDate('date_applied', '<=', $date)
                ->whereDate('date_applied', '=', $carbon_date);
        }

        return view('applications', ['transactions' => $transactions->get(), 'date' => $date]);
    }

    public function viewApplication($transaction_id) {
        $transaction = Transaction::where('id', '=', $transaction_id)->first();

        if ($transaction == null)
            return redirect()->back();

        $appointment = $transaction->appointment;

        if ($transaction->transaction_type_id == 1) {
            return view('applications.view_license', ['drivers_license' => $appointment, 'transaction' => $transaction]);
        } else if ($transaction->transaction_type_id == 2) {
            return view('applications.view_vehicle', ['vehicle_registration' => $appointment, 'transaction' => $transaction]);
        } else {

        }

        return view('applications.view_license', ['drivers_license' => $appointment, 'transaction' => $transaction]);
    }

    public function scheduleApplication($transaction_id, Request $request) {
        $request->validate([
            'date_applied' => 'required|max:255',
        ]);

        $carbon_date_string = (new Carbon($request->date_applied))->toDateString();

        $count = Transaction::whereDate('date', '=', $carbon_date_string)->count();

        if ($count > 300) {
            return redirect()->back()->withErrors(['date' => "Please choose other date. Applicants for that date already exceeded the limit."]);
        }

        $transaction = Transaction::where('id', '=', $transaction_id)->first();

        if ($transaction == null)
            return redirect()->back();

        $transaction->date = new Carbon($request->date_applied);
        $transaction->save();

        if ($transaction == null)
            return redirect()->back();

        Mail::to($transaction->user)->send(new TransactionScheduled($transaction, $request->email_body));

        return redirect()->route('applications');
    }

}
