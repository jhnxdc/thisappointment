<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversLicenseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers_licenses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id');
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('address');
            $table->string('tin')->nullable();
            $table->string('tel_no')->nullable();
            $table->string('nationality');
            $table->string('gender');
            $table->string('dob');
            $table->string('weight')->nullable();
            $table->string('height')->nullable();

            $table->string('toa');

            $table->string('tla');

            $table->string('dsa');

            $table->string('ea');

            $table->string('blood_type')->nullable();
            $table->string('organ_donor');

            $table->string('cs');

            $table->string('hair');

            $table->string('eyes');

            $table->string('built');

            $table->string('complexion');

            $table->string('birth_place');
            $table->string('fathers_name')->nullable();
            $table->string('mothers_name')->nullable();
            $table->string('spouse_name')->nullable();
            $table->string('emp_bus_name')->nullable();
            $table->string('emp_tel_no')->nullable();
            $table->string('emp_bus_add')->nullable();
            $table->string('prev_name')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers_licenses');
    }
}
