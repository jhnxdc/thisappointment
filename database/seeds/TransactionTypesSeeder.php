<?php

use Illuminate\Database\Seeder;

class TransactionTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('transactions_type')->insert([
            'id' => 1,
            'name' => 'Application for Driver\'s License',
        ]);

        DB::table('transactions_type')->insert([
            'id' => 2,
            'name' => 'Vehicle Registration',
        ]);

        DB::table('transactions_type')->insert([
            'id' => 3,
            'name' => 'Others',
        ]);
    }
}
